const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const ListSchema = new Schema({
  id: {
    type: Number,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  postcode: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  features: {
    type: Array,
    required: false
  },
  details: {
    type: Array,
    required: false
  }
});
module.exports = List = mongoose.model("listing", ListSchema);