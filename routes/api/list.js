const express = require("express");
const router = express.Router();
const ListServiceController = require('../../controller/list')

// Load List model
const List = require("../../models/List");

// @route GET api/list
// @desc Listing
// @access Public
router.get("/", ListServiceController.getListing);

module.exports = router;