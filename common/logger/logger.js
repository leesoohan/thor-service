var winston = require('winston')
require('winston-daily-rotate-file')
const { combine, timestamp, prettyPtiny, label } = winston.format;
const { LOG_LEVEL, LOG_PATH } = require('../../config/logInformation.js')

const myCustomLevels = {
    levels:{
        error: 0,
        warn: 1,
        info: 2,
        debug: 3
    }
}

var transport = new (winston.transports.DailyRotateFile)({
    fileName: LOG_PATH + '%DATE%.log',
    datePattern: 'YYYY_MM_DD',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d'
})

transport.on('rotate', function(old, news){
    logger.info('Filename: ${news}')
})
var processId = process.pid
var logger = winston.createLogger({
    level: LOG_LEVEL,
    levels: myCustomLevels.levels,
    format: combine(
        label({ label: 'PID : ' + processId, message: true}),
        timestamp(),
        prettyPrint()
    ),
    transports: [transport]
})

var errorLogger = logger.error
logger.error = function(...param){
    console.log(param)
    errorLogger(param)
}

var infoLogger = logger.info
logger.info = function(...param){
    infoLogger(param)
}

module.exports = logger