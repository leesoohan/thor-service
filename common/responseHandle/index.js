const { requestAccepted } = require('../../common/message/response')
const { SUCCESS_STATUS, FAIL_STATUS } = require('../../config/requestStatus')

/**
* @param {response} response
* @param {Error or object} result
*/

const basicResponse = (resp, result) => {
    if(resp.headerSent)
        return

    if(result instanceof Error){
        resp.status(FAIL_STATUS).send({ errorCode: '1', stack: result.stack })
        return
    }

    if(result instanceof Object || result instanceof String){
        resp.status(SUCCESS_STATUS).send(result)
    }else{
        resp.status(SUCCESS_STATUS).send(result.toString())
    }
}

const arrayResponse = (resp, result) => {
    if(resp.headerSent)
        return

    if(result instanceof Error){
        resp.status(FAIL_STATUS).send({ errorCode: '1', stack: result.stack })
        return
    }

    if(result == null){
        resp.status(SUCCESS_STATUS).send([])
        return
    }

    if(Array.isArray(result)){
        resp.status(SUCCESS_STATUS).send(result)
    }else{
        resp.status(SUCCESS_STATUS).send([result])
    }
}

/** 
 * @param {response} resp
*/
const backgroundResponse = (resp) => {
    return resp.status(SUCCESS_STATUS).send(requestAccepted)
}

module.exports = {
    basicResponse: basicResponse,
    backgroundResponse: backgroundResponse,
    arrayResponse: arrayResponse
}