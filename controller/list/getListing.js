const listingApplication = require('../../application/listing');
//var logger = require('../../common/logger');
const { arrayResponse } = require('../../common/responseHandle');

module.exports = async (req, resp) => {
    //logger.info('Get listing');
    var requestData = Object.assign(req.body, req.params, req.query);
    var result = await listingApplication.getListing(requestData);
    arrayResponse(resp, result);
}